# Gatsby Stock Charts

## Installation

`yarn && cd ios && pod install && cd ..`

## Running

`yarn ios`

`yarn android`

## Figma File

https://www.figma.com/file/s57czdvhWfOsYiCVmSqQmg/robinhood

## Video Demonstrations

[![Dark Mode](https://img.youtube.com/vi/3iGlW1cWQLY/0.jpg)](https://www.youtube.com/watch?v=3iGlW1cWQLY)
[![Light Mode](https://img.youtube.com/vi/o3oVaAWkSZs/0.jpg)](https://www.youtube.com/watch?v=o3oVaAWkSZs)
