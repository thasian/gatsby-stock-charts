import { Platform } from "react-native";
import ReactNativeHapticFeedback from "react-native-haptic-feedback";

export const isIOS = Platform.OS === "ios";

export const invokeHaptic = () => {
  if (isIOS) {
    ReactNativeHapticFeedback.trigger("impactLight", {
      enableVibrateFallback: true,
      ignoreAndroidSystemSettings: false,
    });
  }
};
