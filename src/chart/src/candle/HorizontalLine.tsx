import React from "react";
import { StyleSheet } from "react-native";
import Svg, { Circle } from "react-native-svg";
import {
  CandlestickChartDimensionsContext,
  getY,
  useCandlestickChart,
} from ".";

interface Props {
  dotRadius?: number;
  lineColor?: string;
}

export default React.memo(({ dotRadius = 1, lineColor = "gray" }: Props) => {
  const { width, height } = React.useContext(CandlestickChartDimensionsContext);
  const { domain, xValues, previousDayClose } = useCandlestickChart();

  const candleWidth = width / ((xValues?.length ?? 2) - 1);

  return (
    <Svg style={StyleSheet.absoluteFill}>
      {previousDayClose &&
        xValues &&
        xValues.map((_, i) => {
          return (
            <Circle
              key={`dotted-line-${i}`}
              cx={candleWidth * i - candleWidth / 2 + dotRadius / 2}
              cy={getY({ maxHeight: height, value: previousDayClose, domain })}
              r={dotRadius}
              fill={lineColor}
            />
          );
        })}
    </Svg>
  );
});
