import * as React from "react";
import type { TextProps as RNTextProps } from "react-native";
import AnimateableText from "react-native-animateable-text";

import { useCandlestickChartDatetime } from "./useDatetime";
import { useAnimatedProps } from "react-native-reanimated";

export type CandlestickDatetimeTextProps = {
  format?: any;
  style?: RNTextProps["style"];
};

export function CandlestickChartDatetimeText({
  format,
  ...props
}: CandlestickDatetimeTextProps) {
  const datetime = useCandlestickChartDatetime({ format });
  const animatedProps = useAnimatedProps(() => {
    return {
      text: datetime.value,
    };
  });
  return <AnimateableText animatedProps={animatedProps} {...props} />;
}
