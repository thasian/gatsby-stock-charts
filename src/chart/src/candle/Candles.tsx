import * as React from "react";
import { Svg, SvgProps } from "react-native-svg";

import { CandlestickChartDimensionsContext } from "./Chart";
import { CandlestickChartCandle, CandlestickChartCandleProps } from "./Candle";
import { useCandlestickChart } from "./useCandlestickChart";

type CandlestickChartCandlesProps = SvgProps & {
  width?: number;
  height?: number;
  margin?: CandlestickChartCandleProps["margin"];
  positiveColor?: CandlestickChartCandleProps["positiveColor"];
  negativeColor?: CandlestickChartCandleProps["negativeColor"];
  renderRect?: CandlestickChartCandleProps["renderRect"];
  renderLine?: CandlestickChartCandleProps["renderLine"];
  rectProps?: CandlestickChartCandleProps["rectProps"];
  lineProps?: CandlestickChartCandleProps["lineProps"];
  candleProps?: Partial<CandlestickChartCandleProps>;
};

export function CandlestickChartCandles({
  positiveColor,
  negativeColor,
  rectProps,
  lineProps,
  margin,
  renderRect,
  renderLine,
  candleProps,
  ...props
}: CandlestickChartCandlesProps) {
  const { width, height } = React.useContext(CandlestickChartDimensionsContext);
  const { data, domain, xValues } = useCandlestickChart();

  ////////////////////////////////////////////////

  return (
    <Svg width={width} height={height * 0.85} {...props}>
      {data.map((candle, index) => (
        <CandlestickChartCandle
          key={index as React.Key}
          domain={domain}
          margin={margin}
          maxHeight={height * 0.85}
          width={width / ((xValues?.length || data.length) - 1)}
          positiveColor={positiveColor}
          negativeColor={negativeColor}
          renderRect={renderRect}
          renderLine={renderLine}
          rectProps={rectProps}
          lineProps={lineProps}
          candle={candle}
          index={index}
          {...candleProps}
        />
      ))}
    </Svg>
  );
}
