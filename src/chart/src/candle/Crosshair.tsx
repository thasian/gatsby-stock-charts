import * as React from "react";
import { StyleSheet, ViewProps } from "react-native";
import {
  LongPressGestureHandler,
  LongPressGestureHandlerProps,
} from "react-native-gesture-handler";
import Animated, {
  useAnimatedGestureHandler,
  useSharedValue,
  useAnimatedStyle,
  useAnimatedReaction,
  runOnJS,
} from "react-native-reanimated";

import { CandlestickChartDimensionsContext } from "./Chart";
import { CandlestickChartLine, CandlestickChartLineProps } from "./Line";
import { useCandlestickChart } from "./useCandlestickChart";
import { CandlestickChartCrosshairTooltipContext } from "./CrosshairTooltip";
import { clamp, invokeHaptic } from "../..";

type CandlestickChartCrosshairProps = LongPressGestureHandlerProps & {
  color?: string;
  children?: React.ReactNode;
  enableHapticFeedback?: boolean;
  horizontalCrosshairProps?: Animated.AnimateProps<ViewProps>;
  verticalCrosshairProps?: Animated.AnimateProps<ViewProps>;
  lineProps?: Partial<CandlestickChartLineProps>;
};

export function CandlestickChartCrosshair({
  color,
  enableHapticFeedback = true,
  children,
  horizontalCrosshairProps = {},
  verticalCrosshairProps = {},
  lineProps = {},
  ...props
}: CandlestickChartCrosshairProps) {
  const { width, height } = React.useContext(CandlestickChartDimensionsContext);
  const { currentX, currentY, currentIndex, data, xValues } =
    useCandlestickChart();

  const tooltipPosition = useSharedValue<"left" | "right">("left");

  const opacity = useSharedValue(0);
  const onGestureEvent = useAnimatedGestureHandler({
    onActive: ({ x, y }) => {
      const boundedX = x <= width - 1 ? x : width - 1;
      if (boundedX < 100) {
        tooltipPosition.value = "right";
      } else {
        tooltipPosition.value = "left";
      }

      const valuesLength = xValues?.length ?? data.length;
      opacity.value = 1;
      const step = width / (valuesLength - 1);
      currentIndex.value = clamp(
        Math.floor((boundedX - (boundedX % step) + step / 2) / step),
        0,
        data.length - 1
      );
      currentX.value = currentIndex.value * step + step / 2;
      currentY.value = clamp(y, 0, height * 0.85);
    },
    onEnd: () => {
      opacity.value = 0;
      currentY.value = -1;
      currentX.value = -1;
    },
  });
  const horizontal = useAnimatedStyle(() => ({
    opacity: opacity.value,
    transform: [{ translateY: currentY.value }],
  }));
  const vertical = useAnimatedStyle(() => ({
    opacity: opacity.value,
    transform: [{ translateX: currentX.value }],
  }));

  useAnimatedReaction(
    () => (enableHapticFeedback ? currentIndex.value : 0),
    (data) => {
      if (data !== 0) {
        runOnJS(invokeHaptic)();
      }
    },
    [enableHapticFeedback]
  );

  return (
    <LongPressGestureHandler
      minDurationMs={200}
      onGestureEvent={onGestureEvent as any}
      {...props}
    >
      <Animated.View style={StyleSheet.absoluteFill}>
        <Animated.View
          style={[StyleSheet.absoluteFill, horizontal]}
          {...horizontalCrosshairProps}
        >
          <CandlestickChartLine color={color} x={width} y={0} {...lineProps} />
          <CandlestickChartCrosshairTooltipContext.Provider
            value={{ position: tooltipPosition }}
          >
            {children}
          </CandlestickChartCrosshairTooltipContext.Provider>
        </Animated.View>
        <Animated.View
          style={[StyleSheet.absoluteFill, vertical]}
          {...verticalCrosshairProps}
        >
          <CandlestickChartLine color={color} x={0} y={height} {...lineProps} />
        </Animated.View>
      </Animated.View>
    </LongPressGestureHandler>
  );
}
