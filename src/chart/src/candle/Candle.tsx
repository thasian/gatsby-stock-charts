import React from "react";
import {
  Color,
  Line,
  LineProps,
  NumberProp,
  Rect,
  RectProps,
} from "react-native-svg";

import type { TCandle, TDomain } from "./types";
import { getY, getHeight } from "./utils";

export type CandlestickChartCandleProps = {
  candle: TCandle;
  domain: TDomain;
  maxHeight: number;
  margin?: number;
  positiveColor?: string;
  negativeColor?: string;
  index: number;
  width: number;
  rectProps?: RectProps;
  lineProps?: LineProps;
  renderRect?: ({
    x,
    y,
    width,
    height,
    fill,
  }: {
    x: NumberProp;
    y: NumberProp;
    width: NumberProp;
    height: NumberProp;
    fill: Color;
  }) => React.ReactNode;
  renderLine?: ({
    x1,
    y1,
    x2,
    y2,
    stroke,
    strokeWidth,
  }: {
    x1: NumberProp;
    y1: NumberProp;
    x2: NumberProp;
    y2: NumberProp;
    stroke: Color;
    strokeWidth: NumberProp;
  }) => React.ReactNode;
};

export const CandlestickChartCandle = ({
  candle,
  maxHeight,
  domain,
  margin = 2,
  positiveColor = "#10b981",
  negativeColor = "#ef4444",
  rectProps: overrideRectProps,
  lineProps: overrideLineProps,
  index,
  width,
  renderLine = (props) => <Line {...props} />,
  renderRect = (props) => <Rect {...props} />,
}: CandlestickChartCandleProps) => {
  const { close, open, high, low } = candle;
  const isPositive = close > open;
  const fill = isPositive ? positiveColor : negativeColor;
  const x = index * width;
  const max = Math.max(open, close);
  const min = Math.min(open, close);

  const lineProps = React.useMemo(
    () => ({
      stroke: "black",
      strokeWidth: 1,
      direction: isPositive ? "positive" : "negative",
      x1: x + 0.5 + width / 2,
      y1: getY({ maxHeight, value: low, domain }),
      x2: x + 0.5 + width / 2,
      y2: getY({ maxHeight, value: high, domain }),
      ...overrideLineProps,
    }),
    [domain, high, isPositive, low, maxHeight, overrideLineProps, width, x]
  );

  const rectProps = React.useMemo(
    () => ({
      width: width - margin * 2,
      fill: fill,
      direction: isPositive ? "positive" : "negative",
      x: x + 0.5 + margin,
      y: getY({ maxHeight, value: max, domain }),
      height: getHeight({ maxHeight, value: max - min, domain }),
      ...overrideRectProps,
    }),
    [
      domain,
      fill,
      isPositive,
      margin,
      max,
      maxHeight,
      min,
      overrideRectProps,
      width,
      x,
    ]
  );

  return (
    <>
      {renderLine({
        ...lineProps,
      })}
      {renderRect({
        ...rectProps,
      })}
    </>
  );
};
