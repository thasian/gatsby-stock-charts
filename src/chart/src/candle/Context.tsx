import * as React from "react";
import { useSharedValue } from "react-native-reanimated";
import { getVolumeDomain } from ".";

import type { TContext, TData } from "./types";
import { getDomain } from "./utils";

export const CandlestickChartContext = React.createContext<TContext>({
  currentX: { value: -1 },
  currentY: { value: -1 },
  currentIndex: { value: -1 },
  data: [],
  xValues: [],
  previousDayClose: undefined,
  domain: [0, 0],
  volumeDomain: [0, 0],
});

type CandlestickChartProviderProps = {
  children: React.ReactNode;
  data: TData;
  xValues?: number[];
  previousDayClose?: number;
};

export function CandlestickChartProvider({
  children,
  data = [],
  xValues,
  previousDayClose,
}: CandlestickChartProviderProps) {
  const currentX = useSharedValue(-1);
  const currentY = useSharedValue(-1);
  const currentIndex = useSharedValue(-1);

  const domain = React.useMemo(
    () => getDomain(data, previousDayClose),
    [data, previousDayClose]
  );

  const volumeDomain = React.useMemo(() => getVolumeDomain(data), [data]);

  const contextValue = React.useMemo(
    () => ({
      currentX,
      currentY,
      currentIndex,
      data,
      domain,
      volumeDomain,
      xValues,
      previousDayClose,
    }),
    [
      currentX,
      currentY,
      currentIndex,
      data,
      xValues,
      previousDayClose,
      domain,
      volumeDomain,
    ]
  );

  return (
    <CandlestickChartContext.Provider value={contextValue}>
      {children}
    </CandlestickChartContext.Provider>
  );
}
