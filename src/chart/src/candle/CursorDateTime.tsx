import React, { useCallback, useContext } from "react";
import {
  CandlestickChartDatetimeText,
  CandlestickChartDimensionsContext,
  CandlestickDatetimeTextProps,
  useCandlestickChart,
} from ".";
import Animated, {
  useSharedValue,
  useAnimatedStyle,
  useDerivedValue,
} from "react-native-reanimated";
import { ViewProps } from "react-native";

type CandleChartCursorDateTimeProps = Animated.AnimateProps<ViewProps> & {
  children?: React.ReactNode;
  format: any;
  xGutter?: number;
  lineGutter?: number;
  textProps?: CandlestickDatetimeTextProps;
  textStyle?: CandlestickDatetimeTextProps["style"];
};

export const CursorDateTime = ({
  children,
  format,
  xGutter = 0,
  lineGutter = 4,
  textProps,
  textStyle,
  ...props
}: CandleChartCursorDateTimeProps) => {
  const { width } = useContext(CandlestickChartDimensionsContext);
  const { currentX } = useCandlestickChart();

  const elementWidth = useSharedValue(0);
  const elementHeight = useSharedValue(0);

  const isActive = useDerivedValue(() => {
    return currentX.value !== -1;
  });

  const handleLayout = useCallback(
    (event) => {
      elementWidth.value = event.nativeEvent.layout.width;
      elementHeight.value = event.nativeEvent.layout.height;
    },
    [elementHeight, elementWidth]
  );

  const animatedCursorStyle = useAnimatedStyle(() => {
    let translateXOffset = elementWidth.value / 2;
    if (currentX.value < elementWidth.value / 2 + xGutter) {
      const xOffset = elementWidth.value / 2 + xGutter - currentX.value;
      translateXOffset = translateXOffset - xOffset;
    }
    if (currentX.value > width - elementWidth.value / 2 - xGutter) {
      const xOffset =
        currentX.value - (width - elementWidth.value / 2 - xGutter);
      translateXOffset = translateXOffset + xOffset;
    }

    return {
      transform: [
        { translateX: currentX.value - translateXOffset },
        {
          translateY: -(elementHeight.value + lineGutter),
        },
      ],
      /*
        Don't show text till we are able to measure its dimensions
      */
      opacity:
        isActive.value && elementWidth.value !== 0 && elementHeight.value !== 0
          ? 1
          : 0,
    };
  }, []);

  return (
    <Animated.View
      onLayout={handleLayout}
      {...props}
      style={[
        {
          position: "absolute",
          padding: 4,
          alignSelf: "flex-start",
        },
        animatedCursorStyle,
        props.style,
      ]}
    >
      {children || (
        <CandlestickChartDatetimeText
          format={format}
          style={textStyle}
          {...textProps}
        />
      )}
    </Animated.View>
  );
};
