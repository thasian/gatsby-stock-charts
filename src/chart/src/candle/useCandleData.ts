import { useDerivedValue } from "react-native-reanimated";

import { useCandlestickChart } from "./useCandlestickChart";

export function useCandleData() {
  const { currentIndex, data } = useCandlestickChart();

  const candle = useDerivedValue(() => {
    if (currentIndex.value === -1 || !data[currentIndex.value]) {
      return {
        timestamp: -1,
        low: -1,
        open: -1,
        high: -1,
        close: -1,
        volume: -1,
      };
    }
    return data[currentIndex.value];
  }, [data]);

  return candle;
}
