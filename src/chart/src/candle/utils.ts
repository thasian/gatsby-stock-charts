import { interpolate, Extrapolate } from "react-native-reanimated";

import type { TCandle, TDomain } from "./types";

export function getDomain(
  rows: TCandle[],
  previousDayClose?: number
): [number, number] {
  "worklet";
  const values = rows.reduce<number[]>((acc, { high, low }) => {
    return [...acc, high, low];
  }, []);
  if (previousDayClose) {
    values.push(previousDayClose);
  }
  const min = Math.min(...values);
  const max = Math.max(...values);
  return [min - (max - min) * 0.025, max + (max - min) * 0.025];
}

export function getVolumeDomain(rows: TCandle[]): [number, number] {
  "worklet";
  const values = rows.reduce<number[]>((acc, { volume }) => {
    return [...acc, volume];
  }, []);
  const min = Math.min(...values);
  const max = Math.max(...values);
  return [min - (max - min) * 0.025, max + (max - min) * 0.025];
}

export function getY({
  value,
  domain,
  maxHeight,
  minHeight = 0,
}: {
  value: number;
  domain: TDomain;
  maxHeight: number;
  minHeight?: number;
}) {
  "worklet";
  return interpolate(value, domain, [maxHeight, minHeight], Extrapolate.CLAMP);
}

export function getHeight({
  value,
  domain,
  maxHeight,
}: {
  value: number;
  domain: TDomain;
  maxHeight: number;
}) {
  "worklet";
  return interpolate(
    value,
    [0, Math.max(...domain) - Math.min(...domain)],
    [0, maxHeight],
    Extrapolate.CLAMP
  );
}

export function getPrice({
  y,
  domain,
  maxHeight,
}: {
  y: number;
  domain: TDomain;
  maxHeight: number;
}) {
  "worklet";
  if (y === -1) return -1;
  return interpolate(y, [0, maxHeight], domain.reverse(), Extrapolate.CLAMP);
}
