import type Animated from "react-native-reanimated";

export type TCandle = {
  timestamp: number;
  open: number;
  high: number;
  low: number;
  close: number;
  volume: number;
};
export type TData = Array<TCandle>;
export type TDomain = [number, number];
export type TContext = {
  currentX: Animated.SharedValue<number>;
  currentY: Animated.SharedValue<number>;
  currentIndex: Animated.SharedValue<number>;
  data: TData;
  domain: TDomain;
  volumeDomain: TDomain;
  xValues?: number[];
  previousDayClose?: number;
};
export type TPriceType = "crosshair" | "open" | "close" | "low" | "high";
