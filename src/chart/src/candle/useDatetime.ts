import { useDerivedValue } from "react-native-reanimated";
import { useCandlestickChart } from ".";

export function useCandlestickChartDatetime({
  format,
}: {
  format?: any;
} = {}) {
  const { data, currentIndex } = useCandlestickChart();

  const formatted = useDerivedValue(() => {
    const currentCandle = data[currentIndex.value];

    if (currentIndex.value === -1 || !currentCandle) return "";

    const nextCandle = data[currentIndex.value + 1];
    return format({
      value: currentCandle.timestamp,
      nextValue: nextCandle?.timestamp,
    }) as string;
  }, [data]);

  return formatted;
}
