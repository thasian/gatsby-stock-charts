import * as React from "react";
import { Svg, SvgProps } from "react-native-svg";

import { CandlestickChartDimensionsContext } from "./Chart";
import { CandlestickChartVolume, CandlestickChartVolumeProps } from "./Volume";
import { useCandlestickChart } from "./useCandlestickChart";

type CandlestickChartVolumesProps = SvgProps & {
  width?: number;
  height?: number;
  margin?: CandlestickChartVolumeProps["margin"];
  positiveColor?: CandlestickChartVolumeProps["positiveColor"];
  negativeColor?: CandlestickChartVolumeProps["negativeColor"];
  renderRect?: CandlestickChartVolumeProps["renderRect"];
  rectProps?: CandlestickChartVolumeProps["rectProps"];
  candleProps?: Partial<CandlestickChartVolumeProps>;
};

export function CandlestickChartVolumes({
  positiveColor,
  negativeColor,
  rectProps,
  margin,
  renderRect,
  candleProps,
  ...props
}: CandlestickChartVolumesProps) {
  const { width, height } = React.useContext(CandlestickChartDimensionsContext);
  const { data, volumeDomain, xValues } = useCandlestickChart();

  return (
    <Svg width={width} height={height * 0.15} {...props}>
      {data.map((candle, index) => (
        <CandlestickChartVolume
          key={index as React.Key}
          domain={volumeDomain}
          margin={margin}
          maxHeight={height * 0.15 - 8}
          width={width / ((xValues?.length || data.length) - 1)}
          positiveColor={positiveColor}
          negativeColor={negativeColor}
          renderRect={renderRect}
          rectProps={rectProps}
          candle={candle}
          index={index}
          {...candleProps}
        />
      ))}
    </Svg>
  );
}
