import React from "react";
import { Color, NumberProp, Rect, RectProps } from "react-native-svg";
import { getHeight, getY, TCandle, TDomain } from ".";

export interface CandlestickChartVolumeProps {
  candle: TCandle;
  domain: TDomain;
  maxHeight: number;
  margin?: number;
  positiveColor?: string;
  negativeColor?: string;
  index: number;
  width: number;
  rectProps?: RectProps;
  renderRect?: ({
    x,
    y,
    width,
    height,
    fill,
  }: {
    x: NumberProp;
    y: NumberProp;
    width: NumberProp;
    height: NumberProp;
    fill: Color;
  }) => React.ReactNode;
}

export const CandlestickChartVolume = ({
  candle,
  maxHeight,
  domain,
  margin = 2,
  positiveColor = "#10b981",
  negativeColor = "#ef4444",
  rectProps: overrideRectProps,
  index,
  width,
  renderRect = (props) => <Rect {...props} />,
}: CandlestickChartVolumeProps) => {
  const { close, open, volume } = candle;
  const isPositive = close > open;
  const fill = isPositive ? positiveColor : negativeColor;
  const x = index * width;

  const rectProps = React.useMemo(
    () => ({
      width: width - margin * 2,
      fill: fill,
      direction: isPositive ? "positive" : "negative",
      x: x + 0.5 + margin,
      y: getY({
        maxHeight,
        value: volume,
        domain,
      }),
      height: getHeight({
        maxHeight,
        value: volume - domain[0],
        domain,
      }),
      opacity: 0.4,
      ...overrideRectProps,
    }),
    [
      domain,
      fill,
      isPositive,
      margin,
      maxHeight,
      overrideRectProps,
      width,
      x,
      volume,
    ]
  );

  return <>{renderRect({ ...rectProps })}</>;
};
