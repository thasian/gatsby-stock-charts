import * as React from "react";
import type { TextProps as RNTextProps } from "react-native";
import type { TPriceType } from "./types";

import { useCandlestickChartPrice } from "./usePrice";
import AnimateableText from "react-native-animateable-text";
import { useAnimatedProps } from "react-native-reanimated";

export type CandlestickChartPriceTextProps = {
  format?: any;
  precision?: number;
  type?: TPriceType;
  style?: RNTextProps["style"];
};

export function CandlestickChartPriceText({
  format,
  precision = 2,
  type = "crosshair",
  ...props
}: CandlestickChartPriceTextProps) {
  const price = useCandlestickChartPrice({ format, precision, type });
  const animatedProps = useAnimatedProps(() => {
    return {
      text: price.value,
    };
  });
  return <AnimateableText animatedProps={animatedProps} {...props} />;
}
