import { useMemo } from "react";
import Animated, {
  useSharedValue,
  useAnimatedReaction,
  withTiming,
} from "react-native-reanimated";
import { usePrevious } from "./usePrevious";

export const useTransition = <Val, PVal>(
  value: Val,
  interpolate: (
    transition: number,
    previousValue: PVal,
    value: PVal
  ) => string | number,
  parse: (value: Val) => PVal
) => {
  const transition = useSharedValue(0);

  const parsedValue = useMemo(() => {
    return value ? parse(value) : undefined;
  }, [parse, value]);
  const previousParsedValue = usePrevious(parsedValue);

  useAnimatedReaction(
    () => {
      return value;
    },
    (_, previous) => {
      if (previous) {
        transition.value = 0;
        transition.value = withTiming(1);
      }
    },
    [value]
  );

  return {
    transition,
    getMixedValue: (transition: Animated.SharedValue<any>) => {
      "worklet";

      if (!parsedValue) {
        return "";
      }

      return interpolate(
        transition.value,
        previousParsedValue || parsedValue,
        parsedValue
      );
    },
  };
};

export type Transition = ReturnType<typeof useTransition>;
