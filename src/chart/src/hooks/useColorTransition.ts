import { AnimatedColor, mixColor } from "react-native-redash";
import { useTransition } from "./useTransition";

export const useColorTransition = (color: string) => {
  return useTransition(color, mixColor, (color) => color as AnimatedColor);
};
