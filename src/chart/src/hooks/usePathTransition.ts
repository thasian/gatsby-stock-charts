import { mixPath, parse } from "react-native-redash";
import { useTransition } from "./useTransition";

export const usePathTransition = (path: string) => {
  return useTransition(path, mixPath, parse);
};
