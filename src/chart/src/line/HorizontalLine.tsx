import React from "react";
import { StyleSheet } from "react-native";
import Svg, { Circle } from "react-native-svg";
import { LineChartDimensionsContext, useLineChart } from ".";
import { getY } from "../candle";

interface Props {
  dotRadius?: number;
  lineColor?: string;
}

export default React.memo(({ dotRadius = 1, lineColor = "gray" }: Props) => {
  const { width, height, gutter } = React.useContext(
    LineChartDimensionsContext
  );
  const { domain, xValues, previousDayClose } = useLineChart();

  const candleWidth = width / ((xValues?.length ?? 2) - 1);

  return (
    <Svg style={StyleSheet.absoluteFill}>
      {previousDayClose &&
        xValues &&
        xValues.map((_, i) => {
          if (i % 2 !== 0) {
            return;
          }
          return (
            <Circle
              key={`dotted-line-${i}`}
              cx={candleWidth * i + dotRadius / 2}
              cy={getY({
                maxHeight: height - gutter,
                minHeight: gutter,
                value: previousDayClose,
                domain,
              })}
              r={dotRadius}
              fill={lineColor}
            />
          );
        })}
    </Svg>
  );
});
