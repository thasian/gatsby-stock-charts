import * as React from "react";
import { StyleSheet } from "react-native";
import {
  LongPressGestureHandler,
  LongPressGestureHandlerProps,
} from "react-native-gesture-handler";
import Animated, {
  runOnJS,
  useAnimatedGestureHandler,
  useAnimatedReaction,
} from "react-native-reanimated";
import { getYForX, parse } from "react-native-redash";
import { clamp, invokeHaptic } from "../..";

import { LineChartDimensionsContext } from "./Chart";
import { useLineChart } from "./useLineChart";

type LineChartCursorProps = LongPressGestureHandlerProps & {
  children: React.ReactNode;
  enableHapticFeedback?: boolean;
};

export function LineChartCursor({
  children,
  enableHapticFeedback = true,
  ...props
}: LineChartCursorProps) {
  const { width, path } = React.useContext(LineChartDimensionsContext);
  const { currentX, currentY, currentIndex, isActive, data, xValues } =
    useLineChart();

  const parsedPath = React.useMemo(
    () => (path ? parse(path) : undefined),
    [path]
  );

  const onGestureEvent = useAnimatedGestureHandler({
    onActive: ({ x }) => {
      if (parsedPath) {
        const boundedX = x <= width ? x : width;
        const valuesLength = xValues?.length ?? data.length;
        isActive.value = true;
        currentIndex.value = clamp(
          Math.round(boundedX / width / (1 / (valuesLength - 1))),
          0,
          data.length - 1
        );
        currentX.value = currentIndex.value * (width / (valuesLength - 1));
        currentY.value = getYForX(parsedPath, currentX.value) || 0;
      }
    },
    onEnd: () => {
      isActive.value = false;
      currentIndex.value = -1;
    },
  });

  useAnimatedReaction(
    () => (enableHapticFeedback ? isActive.value : false),
    (data) => {
      if (data) {
        runOnJS(invokeHaptic)();
      }
    },
    [enableHapticFeedback]
  );

  return (
    <LongPressGestureHandler
      minDurationMs={200}
      onGestureEvent={onGestureEvent as any}
      {...props}
    >
      <Animated.View style={StyleSheet.absoluteFill}>{children}</Animated.View>
    </LongPressGestureHandler>
  );
}
