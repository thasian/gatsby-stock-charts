import * as React from "react";
import type Animated from "react-native-reanimated";
import AnimateableText from "react-native-animateable-text";

import { useLineChartDatetime } from "./useDatetime";
import { useAnimatedProps } from "react-native-reanimated";
import { TextProps as RNTextProps } from "react-native";

export type LineChartDatetimeTextProps = {
  locale?: string;
  options?: { [key: string]: any };
  format?: any;
  variant?: "formatted" | "value";
  style?: RNTextProps["style"];
};

export function LineChartDatetimeText({
  locale,
  options,
  format,
  variant = "formatted",
  ...props
}: LineChartDatetimeTextProps) {
  const datetime = useLineChartDatetime({ format, locale, options });
  const animatedProps = useAnimatedProps(() => {
    return {
      text: (datetime[variant] as Animated.SharedValue<string>).value,
    };
  });
  return <AnimateableText animatedProps={animatedProps} {...props} />;
}
