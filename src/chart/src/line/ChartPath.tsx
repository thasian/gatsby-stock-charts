import React, { useContext } from "react";
import { StyleSheet, View } from "react-native";
import { useAnimatedProps, withTiming } from "react-native-reanimated";
import { AnimatedSvg } from "../../animated";

import { LineChartDimensionsContext } from "./Chart";
import { LineChartPath, LineChartPathProps } from "./Path";
import { useLineChart } from "./useLineChart";

type LineChartPathWrapperProps = {
  color?: string;
  width?: number;
  pathProps?: Partial<LineChartPathProps>;
};

export function LineChartPathWrapper({
  color = "black",
  width: pathWidth = 1.5,
  pathProps = {},
}: LineChartPathWrapperProps) {
  const { width, height } = useContext(LineChartDimensionsContext);
  const { currentX, isActive } = useLineChart();

  const activeSvgProps = useAnimatedProps(() => ({
    width: currentX.value,
  }));

  const backingSvgProps = useAnimatedProps(() => ({
    opacity: withTiming(isActive.value ? 0.2 : 1),
  }));

  return (
    <>
      <View style={StyleSheet.absoluteFill}>
        <AnimatedSvg animatedProps={activeSvgProps} height={height * 0.85}>
          <LineChartPath color={color} width={pathWidth} {...pathProps} />
        </AnimatedSvg>
      </View>
      <View style={[{ width, height: height * 0.85 }]}>
        <AnimatedSvg
          animatedProps={backingSvgProps}
          width={width}
          height={height * 0.85}
        >
          <LineChartPath color={color} width={pathWidth} {...pathProps} />
        </AnimatedSvg>
      </View>
    </>
  );
}
