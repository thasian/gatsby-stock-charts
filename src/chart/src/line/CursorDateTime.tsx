import React, { useCallback, useContext } from "react";
import {
  LineChart,
  LineChartDimensionsContext,
  useLineChart,
  LineChartPriceTextProps,
} from ".";
import Animated, {
  useSharedValue,
  useAnimatedStyle,
} from "react-native-reanimated";
import { ViewProps } from "react-native";

export type LineChartCursorDateTimeProps = Animated.AnimateProps<ViewProps> & {
  children?: React.ReactNode;
  format: any;
  xGutter?: number;
  lineGutter?: number;
  textProps?: LineChartDatetimeTextProps;
  textStyle?: LineChartDatetimeTextProps["style"];
};

export const CursorDateTime = ({
  children,
  format,
  xGutter = 0,
  lineGutter = 4,
  textProps,
  textStyle,
  ...props
}: LineChartCursorDateTimeProps) => {
  const { width } = useContext(LineChartDimensionsContext);
  const { currentX, isActive } = useLineChart();

  const elementWidth = useSharedValue(0);
  const elementHeight = useSharedValue(0);

  const handleLayout = useCallback(
    (event) => {
      elementWidth.value = event.nativeEvent.layout.width;
      elementHeight.value = event.nativeEvent.layout.height;
    },
    [elementHeight, elementWidth]
  );

  const animatedCursorStyle = useAnimatedStyle(() => {
    let translateXOffset = elementWidth.value / 2;
    if (currentX.value < elementWidth.value / 2 + xGutter) {
      const xOffset = elementWidth.value / 2 + xGutter - currentX.value;
      translateXOffset = translateXOffset - xOffset;
    }
    if (currentX.value > width - elementWidth.value / 2 - xGutter) {
      const xOffset =
        currentX.value - (width - elementWidth.value / 2 - xGutter);
      translateXOffset = translateXOffset + xOffset;
    }

    return {
      transform: [
        { translateX: currentX.value - translateXOffset },
        {
          translateY: -(elementHeight.value + lineGutter),
        },
      ],
      /*
        Don't show text till we are able to measure its dimensions
      */
      opacity:
        isActive.value && elementWidth.value !== 0 && elementHeight.value !== 0
          ? 1
          : 0,
    };
  }, []);

  return (
    <Animated.View
      onLayout={handleLayout}
      {...props}
      style={[
        {
          position: "absolute",
          padding: 4,
          alignSelf: "flex-start",
        },
        animatedCursorStyle,
        props.style,
      ]}
    >
      {children || (
        <LineChart.DatetimeText
          format={format}
          style={textStyle}
          {...textProps}
        />
      )}
    </Animated.View>
  );
};
