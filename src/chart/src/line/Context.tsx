import React, { useMemo } from "react";
import { useSharedValue } from "react-native-reanimated";

import type { TLineChartContext, TLineChartData } from "./types";
import { getDomain } from "./utils";

export const LineChartContext = React.createContext<TLineChartContext>({
  currentX: { value: -1 },
  currentY: { value: -1 },
  currentIndex: { value: -1 },
  data: [],
  xValues: [],
  previousDayClose: undefined,
  domain: [0, 0],
  isActive: { value: false },
});

type LineChartProviderProps = {
  children: React.ReactNode;
  xValues?: number[];
  previousDayClose?: number;
  data: TLineChartData;
};

export function LineChartProvider({
  children,
  xValues,
  previousDayClose,
  data = [],
}: LineChartProviderProps) {
  const currentX = useSharedValue(-1);
  const currentY = useSharedValue(-1);
  const currentIndex = useSharedValue(-1);
  const isActive = useSharedValue(false);

  const domain = useMemo(
    () => getDomain(data, previousDayClose),
    [data, previousDayClose]
  );

  const contextValue = useMemo(
    () => ({
      currentX,
      currentY,
      currentIndex,
      xValues,
      previousDayClose,
      data,
      isActive,
      domain,
    }),
    [
      currentIndex,
      currentX,
      currentY,
      data,
      xValues,
      previousDayClose,
      domain,
      isActive,
    ]
  );

  return (
    <LineChartContext.Provider value={contextValue}>
      {children}
    </LineChartContext.Provider>
  );
}
