import { line } from "d3-shape";
import { scaleLinear } from "d3-scale";

import type { TLineChartData, TLineChartPoint } from "./types";

export function getDomain(
  rows: TLineChartPoint[],
  previousDayClose?: number
): [number, number] {
  "worklet";
  const values = rows.map(({ value }) => value);
  if (previousDayClose) {
    values.push(previousDayClose);
  }
  return [Math.min(...values), Math.max(...values)];
}

export function getPath({
  data,
  xValues,
  previousDayClose,
  width,
  height,
  gutter,
}: {
  data: TLineChartData;
  xValues?: number[];
  previousDayClose?: number;
  width: number;
  height: number;
  gutter: number;
}): string {
  const timestamps = (xValues || data).map(({}, i) => i);
  const values = data.map(({ value }) => value);
  if (previousDayClose) {
    values.push(previousDayClose);
  }
  const scaleX = scaleLinear()
    .domain([Math.min(...timestamps), Math.max(...timestamps)])
    .range([0, width]);
  const scaleY = scaleLinear()
    .domain([Math.min(...values), Math.max(...values)])
    .range([height - gutter, gutter]);
  const path = line<TLineChartPoint>()
    .x((_, i) => scaleX(i))
    .y(({ value }) => scaleY(value))(data) as string;
  return path;
}
