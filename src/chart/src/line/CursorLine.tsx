import React from "react";
import { StyleSheet } from "react-native";
import Animated, {
  useAnimatedProps,
  useAnimatedStyle,
} from "react-native-reanimated";
import Svg, { LineProps } from "react-native-svg";
import { AnimatedLine } from "../../animated";
import { useColorTransition } from "../hooks";

import { LineChartDimensionsContext } from "./Chart";
import { LineChartCursor } from "./Cursor";
import { useLineChart } from "./useLineChart";

type LineChartCursorLineProps = {
  children?: React.ReactNode;
  color?: string;
  lineProps?: Partial<LineProps>;
};

export const LineChartCursorLine = ({
  children,
  color = "gray",
  lineProps = {},
}: LineChartCursorLineProps) => {
  const { height } = React.useContext(LineChartDimensionsContext);
  const { currentX, isActive } = useLineChart();

  const vertical = useAnimatedStyle(() => ({
    opacity: isActive.value ? 1 : 0,
    height: "100%",
    transform: [{ translateX: currentX.value }],
  }));

  const { transition, getMixedValue } = useColorTransition(color);

  const animatedProps = useAnimatedProps(() => {
    return {
      stroke: getMixedValue(transition),
    };
  });

  return (
    <LineChartCursor>
      <Animated.View style={vertical}>
        <Svg style={StyleSheet.absoluteFill}>
          <AnimatedLine
            animatedProps={animatedProps}
            x1={0}
            y1={0}
            x2={0}
            y2={height}
            strokeWidth={2}
            strokeDasharray="3 3"
            {...lineProps}
          />
        </Svg>
      </Animated.View>
      {children}
    </LineChartCursor>
  );
};
