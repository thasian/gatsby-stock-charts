import * as React from "react";
import Animated, { useAnimatedProps } from "react-native-reanimated";
import { PathProps } from "react-native-svg";

import { LineChartDimensionsContext } from "./Chart";
import { AnimatedPath } from "../../animated";
import { useColorTransition } from "../hooks/useColorTransition";

export type LineChartPathProps = Animated.AnimateProps<PathProps> & {
  color: string;
  width: number;
};

export function LineChartPath({ color, width, ...props }: LineChartPathProps) {
  const { path } = React.useContext(LineChartDimensionsContext);

  const { transition: colorTransition, getMixedValue: getColorMixedValue } =
    useColorTransition(color);

  ////////////////////////////////////////////////

  const animatedProps = useAnimatedProps(() => {
    return {
      stroke: getColorMixedValue(colorTransition) as string,
    };
  });

  ////////////////////////////////////////////////

  return (
    <>
      <AnimatedPath
        d={path}
        animatedProps={animatedProps}
        fill="transparent"
        strokeWidth={width}
        {...props}
      />
    </>
  );
}
