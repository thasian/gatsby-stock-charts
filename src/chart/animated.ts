import Animated from "react-native-reanimated";
import Svg, { Line, Path, Rect } from "react-native-svg";

export const AnimatedRect = Animated.createAnimatedComponent(Rect);
export const AnimatedLine = Animated.createAnimatedComponent(Line);
export const AnimatedPath = Animated.createAnimatedComponent(Path);
export const AnimatedSvg = Animated.createAnimatedComponent(Svg);
