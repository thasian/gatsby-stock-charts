import React, { useContext } from "react";
import { StyleSheet, View } from "react-native";
import Animated, {
  useAnimatedReaction,
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
  withTiming,
} from "react-native-reanimated";
import Row from "./Row";
import { Colors } from "../theme";
import { PlotType, toBigNumber, toPercent, toUSD, useScreen } from "../utils";
import { ThemeContext } from "./Theme";
import { useCandleData, useCandlestickChart } from "../chart";

const PADDING_HORIZONTAL = 26;
const PADDING_VERTICAL = 10;

const styles = StyleSheet.create({
  table: {
    flexDirection: "row",
    paddingHorizontal: PADDING_HORIZONTAL,
    paddingVertical: PADDING_VERTICAL,
  },
  column: {
    flex: 1,
  },
  separator: {
    width: PADDING_HORIZONTAL,
  },
});

interface Props {
  fadeInDuration?: number;
  fadeOutDuration?: number;
}

export default React.memo(
  ({ fadeInDuration = 100, fadeOutDuration = 200 }: Props) => {
    const {
      theme: { backgroundColor },
    } = useContext(ThemeContext);

    const { currentPlotType } = useScreen();

    const { currentX } = useCandlestickChart();
    const candle = useCandleData();

    const opacity = useSharedValue(0);

    useAnimatedReaction(
      () => {
        return currentX.value;
      },
      (current, previous) => {
        if (current !== -1 && previous == -1) {
          opacity.value = withTiming(1, { duration: fadeInDuration });
        } else if (current === -1 && previous != -1) {
          opacity.value = withTiming(0, { duration: fadeOutDuration });
        }
      },
      []
    );

    const animatedStyle = useAnimatedStyle(() => {
      return {
        opacity: opacity.value,
      };
    }, []);

    const open = useDerivedValue(() => {
      return toUSD(candle.value.open);
    });

    const close = useDerivedValue(() => {
      return toUSD(candle.value.close);
    });

    const volume = useDerivedValue(() => {
      return toBigNumber(candle.value.volume);
    });

    const low = useDerivedValue(() => {
      return toUSD(candle.value.low);
    });

    const high = useDerivedValue(() => {
      return toUSD(candle.value.high);
    });

    const change = useDerivedValue(() => {
      const { close, open } = candle.value;
      return toPercent((close - open) / open);
    });

    const changeColor = useDerivedValue(() => {
      const { close, open } = candle.value;
      return close - open >= 0 ? Colors.green : Colors.red;
    });

    return (
      <Animated.View
        style={[
          StyleSheet.absoluteFill,
          {
            backgroundColor,
          },
          animatedStyle,
        ]}
      >
        {currentPlotType === PlotType.candle && (
          <View style={styles.table}>
            <View style={styles.column}>
              <Row label="Open" value={open} />
              <Row label="High" value={high} />
              <Row label="Volume" value={volume} />
            </View>
            <View style={styles.separator} />
            <View style={styles.column}>
              <Row label="Close" value={close} />
              <Row label="Low" value={low} />
              <Row label="Change" value={change} color={changeColor} />
            </View>
          </View>
        )}
      </Animated.View>
    );
  }
);
