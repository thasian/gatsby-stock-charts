import React from "react";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { StyleSheet, Text } from "react-native";
import Animated, { useAnimatedStyle } from "react-native-reanimated";

import { Typography } from "../theme";
import { Transition } from "../chart";
import { useTheme } from "../utils";

const BUTTON_BORDER_RADIUS = 6;
const BUTTON_HORIZONTAL_PADDING = 8;
const BUTTON_VERTICAL_PADDING = 5;

const styles = StyleSheet.create({
  labelContainer: {
    borderRadius: BUTTON_BORDER_RADIUS,
    paddingHorizontal: BUTTON_HORIZONTAL_PADDING,
    paddingVertical: BUTTON_VERTICAL_PADDING,
  },
});

interface SelectionProps extends Transition {
  color: string;
  label: string;
  isActive: boolean;
  onTap: () => void;
}

export default ({
  color,
  isActive,
  label,
  onTap,
  transition,
  getMixedValue,
}: SelectionProps) => {
  const {
    theme: { backgroundColor },
  } = useTheme();

  const animatedStyle = useAnimatedStyle(() => {
    return {
      backgroundColor: isActive ? getMixedValue(transition) : "transparent",
    };
  });

  return (
    <TouchableWithoutFeedback onPress={() => onTap()}>
      <Animated.View style={[styles.labelContainer, animatedStyle]}>
        <Text
          style={[
            Typography.RegularBold,
            { color: isActive ? backgroundColor : color },
          ]}
        >
          {label}
        </Text>
      </Animated.View>
    </TouchableWithoutFeedback>
  );
};
