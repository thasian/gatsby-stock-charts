import React, { useContext } from "react";
import { StyleSheet, Text, View, ViewStyle } from "react-native";
import { Typography } from "../theme";
import { ThemeContext } from "./Theme";
import LeftArrow from "../assets/images/left-arrow.svg";
import Star from "../assets/images/star.svg";

const styles = StyleSheet.create({
  navigation: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
});

interface Props {
  stockTicker: string;
  style?: ViewStyle;
}

export default ({ stockTicker, style }: Props) => {
  const {
    theme: { textColor },
  } = useContext(ThemeContext);

  return (
    <View style={[styles.navigation, style]}>
      <LeftArrow width={20} height={20} fill={textColor} />
      <View>
        <Text
          style={[Typography.H2, { color: textColor }]}
        >{` ${stockTicker}`}</Text>
      </View>
      <Star width={20} height={20} stroke={"#2B2C26"} />
    </View>
  );
};
