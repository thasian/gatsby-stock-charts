import React from "react";
import { StyleSheet, View } from "react-native";

import { useColorTransition } from "../chart";
import { Duration, PlotType, useScreen } from "../utils";
import Selection from "./Selection";
import Toggle from "./Toggle";

const TOGGLE_SIZE = 20;

const styles = StyleSheet.create({
  selection: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-evenly",
  },
});

interface Props {
  durations: Duration[];
  color: string;
}

export default React.memo(({ durations, color }: Props) => {
  const {
    currentDuration,
    setCurrentDuration,
    currentPlotType,
    setCurrentPlotType,
  } = useScreen();

  const { transition, getMixedValue } = useColorTransition(color);

  return (
    <View style={styles.selection}>
      {durations.map((duration) => {
        return (
          <Selection
            key={duration}
            label={duration}
            isActive={currentDuration === duration}
            onTap={() => setCurrentDuration(duration)}
            color={color}
            transition={transition}
            getMixedValue={getMixedValue}
          />
        );
      })}
      <Toggle
        height={TOGGLE_SIZE}
        width={TOGGLE_SIZE}
        onTap={() =>
          setCurrentPlotType(
            currentPlotType === PlotType.line ? PlotType.candle : PlotType.line
          )
        }
        transition={transition}
        getMixedValue={getMixedValue}
        plotType={currentPlotType}
      />
    </View>
  );
});
