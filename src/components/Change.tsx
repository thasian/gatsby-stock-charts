import React, { useContext } from "react";
import { StyleSheet, View } from "react-native";
import { Colors, Typography } from "../theme";
import { ThemeContext } from "./Theme";
import { PlotType } from "../utils";
import UpArrow from "../assets/images/up-arrow.svg";
import DownArrow from "../assets/images/down-arrow.svg";
import Animated, {
  useAnimatedProps,
  useAnimatedReaction,
  useAnimatedStyle,
  useDerivedValue,
  useSharedValue,
} from "react-native-reanimated";
import { useLineChart } from "../chart";
import AnimateableText from "react-native-animateable-text";
import { useScreen } from "../utils/useScreen";

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    paddingVertical: 3,
  },
});

export default () => {
  const {
    theme: { textColor },
  } = useContext(ThemeContext);

  const { currentPlotType, label } = useScreen();
  const { currentIndex, data } = useLineChart();

  const openPrice = data[0]?.value || 1;

  const dollarChange = useDerivedValue(() => {
    if (currentIndex.value === -1 || currentPlotType === PlotType.candle) {
      const last = data[data.length - 1]?.value || 1;
      return last - openPrice;
    }
    const current = data[currentIndex.value].value;
    return current - openPrice;
  });

  const dollarPercent = useDerivedValue(() => {
    return (dollarChange.value / openPrice) * 100;
  });

  /*
    Hack for properly rendering the initial color
    for the animated text.
    Open issue for this: https://github.com/axelra-ag/react-native-animateable-text/issues/20
  */
  const color = useSharedValue("black");
  useAnimatedReaction(
    () => {
      return dollarChange.value >= 0 ? Colors.green : Colors.red;
    },
    (newColor) => {
      color.value = newColor;
    },
    []
  );

  const changeText = useDerivedValue(() => {
    return `$${Math.abs(dollarChange.value).toFixed(2)} (${Math.abs(
      dollarPercent.value
    ).toFixed(2)}%) `;
  });

  const labelText = useDerivedValue(() => {
    return currentIndex.value !== -1 ? "" : label;
  });

  const arrowStyle = useAnimatedStyle(() => ({
    backgroundColor: "white",
    opacity: dollarChange.value >= 0 ? 0 : 1,
  }));

  const numberAnimatedProps = useAnimatedProps(() => {
    return {
      text: changeText.value,
      color: color.value,
    };
  });

  const labelAnimatedProps = useAnimatedProps(() => {
    return {
      text: labelText.value,
    };
  });

  return (
    <View style={styles.container}>
      <View>
        <UpArrow width={10} height={10} fill={Colors.green} />
        <Animated.View style={[StyleSheet.absoluteFill, arrowStyle]}>
          <DownArrow width={10} height={10} fill={Colors.red} />
        </Animated.View>
      </View>

      <AnimateableText
        animatedProps={numberAnimatedProps}
        style={[Typography.Regular, { paddingLeft: 2 }]}
      />
      <AnimateableText
        animatedProps={labelAnimatedProps}
        style={[Typography.Regular, { color: textColor }]}
      />
    </View>
  );
};
