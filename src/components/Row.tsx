import React, { useContext } from "react";
import { View, Text, StyleSheet } from "react-native";
import AnimateableText from "react-native-animateable-text";
import Animated, { useAnimatedProps } from "react-native-reanimated";

import { Typography } from "../theme";
import { ThemeContext } from "./Theme";

const VERTICAL_MARGIN = 2;

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: VERTICAL_MARGIN,
  },
});

interface RowProps {
  label: string;
  value: Animated.SharedValue<string>;
  color?: Animated.SharedValue<string>;
}

export default ({ label, value, color }: RowProps) => {
  const {
    theme: { textColor },
  } = useContext(ThemeContext);

  const animatedProps = useAnimatedProps(() => {
    return {
      text: value.value,
      color: color?.value || textColor,
    };
  });

  return (
    <View style={styles.container}>
      <Text style={[Typography.Caption, { color: textColor }]}>{label}</Text>
      <AnimateableText
        animatedProps={animatedProps}
        style={Typography.Regular}
      />
    </View>
  );
};
