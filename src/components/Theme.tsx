import React, { useEffect } from "react";
import { Appearance } from "react-native";

const themes = {
  dark: {
    backgroundColor: "black",
    textColor: "white",
  },
  light: {
    backgroundColor: "white",
    textColor: "black",
  },
};

const initialState = {
  theme: themes.light,
};

const ThemeContext = React.createContext(initialState);

interface ThemeProviderProps {
  children: React.ReactNode;
}

const ThemeProvider = ({ children }: ThemeProviderProps) => {
  const [isDark, setIsDark] = React.useState(false);

  useEffect(() => {
    setIsDark(Appearance.getColorScheme() === "dark");

    const listener = (pref: Appearance.AppearancePreferences) => {
      setIsDark(pref.colorScheme === "dark");
    };
    Appearance.addChangeListener(listener);

    return () => {
      Appearance.removeChangeListener(listener);
    };
  }, []);

  const theme = isDark ? themes.dark : themes.light;

  return (
    <ThemeContext.Provider value={{ theme }}>{children}</ThemeContext.Provider>
  );
};

export { ThemeProvider, ThemeContext };
