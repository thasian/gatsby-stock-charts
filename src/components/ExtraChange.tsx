import React, { useContext } from "react";
import { StyleSheet, Text } from "react-native";
import { Colors, Typography } from "../theme";
import { ThemeContext } from "./Theme";
import UpArrow from "../assets/images/up-arrow.svg";
import DownArrow from "../assets/images/down-arrow.svg";
import Animated, { useAnimatedStyle } from "react-native-reanimated";
import { useLineChart } from "../chart";

const styles = StyleSheet.create({
  container: {
    alignItems: "center",
    flexDirection: "row",
    paddingVertical: 3,
  },
});

interface Props {
  change: number;
  percent: number;
  label: string;
}

export default React.memo(({ change, percent, label }: Props) => {
  const {
    theme: { textColor },
  } = useContext(ThemeContext);

  const { isActive } = useLineChart();

  const isPositive = change >= 0;
  const color = isPositive ? Colors.green : Colors.red;

  const changeText = `$${Math.abs(change).toFixed(2)} (${Math.abs(
    percent
  ).toFixed(2)}%) `;

  const containerStyle = useAnimatedStyle(() => ({
    opacity: isActive.value ? 0 : 1,
  }));

  return (
    <Animated.View
      key={`percent-change-${label}`}
      style={[styles.container, containerStyle]}
    >
      {isPositive && <UpArrow width={10} height={10} fill={Colors.green} />}
      {!isPositive && <DownArrow width={10} height={10} fill={Colors.red} />}

      <Text style={[Typography.Regular, { color, paddingLeft: 2 }]}>
        {changeText}
      </Text>
      <Text style={[Typography.Regular, { color: textColor }]}>{label}</Text>
    </Animated.View>
  );
});
