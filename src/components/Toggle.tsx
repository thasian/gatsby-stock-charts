import React from "react";
import { TouchableWithoutFeedback } from "react-native-gesture-handler";
import { useAnimatedProps } from "react-native-reanimated";
import Svg from "react-native-svg";

import CandleChartIcon from "../assets/images/candle-chart.svg";
import { DimensionProps, PlotType, useTheme } from "../utils";
import { AnimatedPath, Transition } from "../chart";
import { StyleSheet, View } from "react-native";

interface Props extends DimensionProps, Transition {
  plotType: PlotType;
  onTap: () => void;
}

export default ({
  getMixedValue,
  transition,
  plotType,
  onTap,
  height,
  width,
}: Props) => {
  const {
    theme: { backgroundColor },
  } = useTheme();
  const animatedProps = useAnimatedProps(() => {
    return {
      fill: getMixedValue(transition),
      stroke: getMixedValue(transition),
    };
  });

  return (
    <TouchableWithoutFeedback onPress={() => onTap()}>
      <>
        <Svg width={width} height={height} viewBox={"0 0 21 18"}>
          <AnimatedPath
            animatedProps={animatedProps}
            d={
              "M3.87382 10.2179H0.5V8.54702H5.0627L7.63323 13.0776L13.2884 0L17.8511 9.09326H18.1403H21V10.7962H17.0157L13.2884 3.92006L7.63323 17.0298L3.87382 10.2179Z"
            }
            strokeWidth={0.2}
          />
        </Svg>
        <View
          style={[
            StyleSheet.absoluteFill,
            {
              backgroundColor,
              opacity: plotType === PlotType.candle ? 1 : 0,
            },
          ]}
        >
          <CandleChartIcon height={height} width={width} />
        </View>
      </>
    </TouchableWithoutFeedback>
  );
};
