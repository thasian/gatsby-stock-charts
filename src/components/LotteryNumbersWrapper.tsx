import React, { useContext } from "react";
import { StyleSheet, Text, View } from "react-native";
import { useDerivedValue } from "react-native-reanimated";

import LotteryNumbers from "./LotteryNumbers";
import { Typography } from "../theme";
import { ThemeContext } from "./Theme";
import { Duration, PlotType, useScreen } from "../utils";
import { useLineChart } from "../chart";

const styles = StyleSheet.create({
  lotteryNumbersContainer: {
    /*
      Arbitrary width that should be able to handle
      every case of stock numbers. ie $3.50 or $534,312.20
    */
    width: 300,
  },
  priceContainer: {
    alignItems: "center",
    flexDirection: "row",
    marginVertical: 10,
  },
});

export default React.memo(() => {
  const {
    theme: { textColor },
  } = useContext(ThemeContext);

  const { currentDuration, currentPlotType } = useScreen();
  const { currentIndex, data } = useLineChart();

  const currentPrice = useDerivedValue(() => {
    if (currentIndex.value === -1 || currentPlotType === PlotType.candle) {
      return data[data.length - 1].value;
    }
    return data[currentIndex.value].value;
  }, [currentPlotType, data]);

  const useValueState = useDerivedValue(() => {
    return currentDuration === Duration.day && currentIndex.value === -1;
  }, [currentDuration]);

  return (
    <View style={styles.priceContainer}>
      <Text style={[Typography.Title, { color: textColor }]}>{"$"}</Text>
      <LotteryNumbers
        value={currentPrice}
        fractionDigits={2}
        style={styles.lotteryNumbersContainer}
        useValueState={useValueState}
      />
    </View>
  );
});
