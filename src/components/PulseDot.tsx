import React, { useEffect } from "react";
import { StyleSheet } from "react-native";
import Animated, {
  Easing,
  interpolate,
  useAnimatedProps,
  useDerivedValue,
  useSharedValue,
  withRepeat,
  withSequence,
  withTiming,
} from "react-native-reanimated";
import Svg, { Circle } from "react-native-svg";
import { getY, LineChartDimensionsContext, useLineChart } from "../chart";
import { useScreen } from "../utils";

const usePulseAnimation = (
  minValue: number,
  maxValue: number,
  animationDuration: number = 900
) => {
  const pulse = useSharedValue(0);

  useEffect(() => {
    pulse.value = withRepeat(
      withSequence(
        withTiming(maxValue, {
          duration: animationDuration,
          easing: Easing.out(Easing.ease),
        }),
        withTiming(minValue, {
          duration: 0,
        })
      ),
      -1 // repeat until cancelled
    );
  }, []);

  return pulse;
};

const AnimatedCircle = Animated.createAnimatedComponent(Circle);

interface Props {
  circleRadius?: number;
  pulseRadius?: number;
}

export default React.memo(
  ({ circleRadius = 3, pulseRadius = circleRadius * 5 }: Props) => {
    const pulse = usePulseAnimation(0, pulseRadius);
    const { width, height, gutter } = React.useContext(
      LineChartDimensionsContext
    );
    const { activeColor } = useScreen();
    const { data, domain, xValues } = useLineChart();

    const x = useDerivedValue(() => {
      const candleWidth = width / ((xValues?.length ?? 2) - 1);
      return candleWidth * (data.length - 1);
    });

    const y = useDerivedValue(() => {
      return getY({
        maxHeight: height * 0.85 - gutter,
        minHeight: gutter,
        value: data[data.length - 1].value,
        domain,
      });
    });

    const regularProps = useAnimatedProps(() => {
      return {
        x: x.value + 0.5,
        y: y.value,
      };
    }, []);

    const animatedProps = useAnimatedProps(() => {
      return {
        x: x.value + 0.5,
        y: y.value,
        r: pulse.value,
        opacity: interpolate(pulse.value, [0, pulseRadius], [1, 0]),
      };
    }, [pulseRadius]);

    return (
      <Svg style={StyleSheet.absoluteFill}>
        <AnimatedCircle
          animatedProps={regularProps}
          fill={activeColor}
          r={circleRadius}
        />
        <AnimatedCircle animatedProps={animatedProps} fill={activeColor} />
      </Svg>
    );
  }
);
