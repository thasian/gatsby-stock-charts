import MaskedView from "@react-native-masked-view/masked-view";
import React, { useState, useEffect } from "react";
import { useRef } from "react";
import { StyleSheet, Text, View, ViewStyle } from "react-native";
import AnimateableText from "react-native-animateable-text";
import LinearGradient from "react-native-linear-gradient";
import Animated, {
  useAnimatedStyle,
  withTiming,
  useSharedValue,
  Easing,
  runOnJS,
  useAnimatedReaction,
  useDerivedValue,
  useAnimatedProps,
} from "react-native-reanimated";
import { Colors, Typography } from "../theme";
import { DimensionProps, modf, range, round } from "../utils";

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  element: {
    position: "absolute",
  },
  measure: {
    opacity: 0,
  },
  tenDigitContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
});

const MeasureElement = React.memo(({ text }: MeasureElementProps) => {
  return (
    <Text
      style={[
        Typography.Title,
        // Font inconsistent so have to hack this
        { paddingRight: text === "1" ? 4 : text === "2" ? 2 : 0 },
      ]}
    >
      {text}
    </Text>
  );
});

const Element = React.memo(({ text, color }: ElementProps) => {
  const animatedProps = useAnimatedProps(() => {
    return {
      text,
      color: color.value,
    };
  }, [text]);

  return (
    <AnimateableText
      animatedProps={animatedProps}
      style={[
        Typography.Title,
        // Font inconsistent so have to hack this
        { paddingRight: text === "1" ? 4 : text === "2" ? 2 : 0 },
      ]}
    />
  );
});

interface Props {
  value: Animated.SharedValue<number>;
  fractionDigits?: number;
  animationDuration?: number;
  withGradient?: boolean;
  useValueState?: Animated.SharedValue<boolean>;
  style?: ViewStyle;
  debug?: boolean;
}

enum VisualElementKind {
  digit,
  comma,
  dot,
  decimalDigit,
}

interface Digit {
  kind: VisualElementKind.digit;
  value: number;
  position: number;
}

interface Comma {
  kind: VisualElementKind.comma;
  position: number;
  visible: boolean;
}

interface Dot {
  kind: VisualElementKind.dot;
  visible: boolean;
}

interface DecimalDigit {
  kind: VisualElementKind.decimalDigit;
  value: number;
  position: number;
}

type VisualElementType = Digit | Comma | Dot | DecimalDigit;

export enum ValueState {
  increase,
  decrease,
  same,
}

const visualElementId = (type: VisualElementType): number => {
  // - Digit has id as a multiple of 10s,
  // - Decimal place has a negative multiple of 10s
  // - 0 is reserved for dot.
  // - 1 is reserved for minus
  // - comma is the id of previous digit + 5.
  //
  // I.e. -1,234.56 -> [1(minus), 40, 35, 30, 20, 10, 0(dot), -10, -20]
  switch (type.kind) {
    case VisualElementKind.digit:
      return 10 * type.position;
    case VisualElementKind.comma:
      // Give comma the id between two digits.
      return 10 * type.position + 5;
    case VisualElementKind.decimalDigit:
      return -10 * type.position;
    case VisualElementKind.dot:
      // Reserve 0 for dot
      return 0;
  }
};

const visualElementString = (type: VisualElementType): string => {
  // - Digit has id as a multiple of 10s,
  // - Decimal place has a negative multiple of 10s
  // - 0 is reserved for dot.
  // - 1 is reserved for minus
  // - comma is the id of previous digit + 5.
  //
  // I.e. -1,234.56 -> [1(minus), 40, 35, 30, 20, 10, 0(dot), -10, -20]
  switch (type.kind) {
    case VisualElementKind.digit:
    case VisualElementKind.decimalDigit:
      return type.value === -1 ? "0" : type.value.toString();
    case VisualElementKind.comma:
      return ",";
    case VisualElementKind.dot:
      return ".";
  }
};

const getAllDigitsInAscendingSignificance = (n: number): number[] => {
  "worklet";

  if (n === 0) {
    return [0];
  }

  let digits: number[] = [];

  while (n >= 10) {
    const quotient = Math.floor(n / 10);
    const digit = n - quotient * 10;
    digits.push(digit);
    n = quotient;
  }

  if (n != 0) {
    digits.push(n);
  }

  return digits;
};

const getWholeVisualElements = (whole: number): VisualElementType[] => {
  "worklet";

  const wholeDigits = getAllDigitsInAscendingSignificance(whole);

  let wholeElements: VisualElementType[] = [];

  let j = 1;
  for (let i = 0; i < wholeDigits.length; i++) {
    if (i !== 0 && i % 3 === 0) {
      wholeElements.push({
        kind: VisualElementKind.comma,
        position: j++,
        visible: true,
      });
    }
    wholeElements.push({
      kind: VisualElementKind.digit,
      value: wholeDigits[i],
      position: j++,
    } as Digit);
  }

  return wholeElements.reverse();
};

const getDecimalVisualElements = (
  fraction: number,
  nFractionDigits: number
): VisualElementType[] => {
  "worklet";
  const decimals = Math.round(fraction * Math.pow(10, nFractionDigits));
  // [5, 4]
  const fractionDigits = getAllDigitsInAscendingSignificance(decimals);

  // Prepend with 0s that are gone when fraction is 0
  const numberOfAdditionalZeroes = nFractionDigits - fractionDigits.length;
  if (numberOfAdditionalZeroes > 0) {
    const zeroes = new Array(numberOfAdditionalZeroes).fill(0);
    fractionDigits.push(...zeroes);
  }

  let fractionElements: VisualElementType[] = [];

  // Work from least significant: [4, 5]
  for (let i = fractionDigits.length - 1; i >= 0; i--) {
    fractionElements.push({
      kind: VisualElementKind.decimalDigit,
      value: fractionDigits[i],
      position: fractionDigits.length - i,
    } as DecimalDigit);
  }

  return fractionElements;
};

const DIGIT_LIST = range(0, 10, 1).reverse();

export interface MeasureElementProps {
  text: string;
}

export interface ElementProps {
  text: string;
  color: Animated.SharedValue<string>;
}

interface TenDigitStackProps {
  state: Animated.SharedValue<ValueState>;
}

const TenDigitStack = React.memo(({ state }: TenDigitStackProps) => {
  const color = useDerivedValue(() => {
    return state.value === ValueState.increase
      ? Colors.green
      : state.value === ValueState.decrease
      ? Colors.red
      : "black";
  }, []);

  return (
    <>
      {DIGIT_LIST.map((iDigit) => {
        return <Element key={iDigit} text={`${iDigit}`} color={color} />;
      })}
    </>
  );
});

const useUpdateDigitY = (digit: number, textHeight: number) => {
  "worklet";

  const y = useSharedValue(-textHeight * 10);

  useAnimatedReaction(
    () => {
      return -textHeight * (9 - digit);
    },
    (current) => {
      y.value = current;
    },
    [textHeight, digit]
  );

  return y;
};

const useUpdateSymbolY = (isVisible: boolean, textHeight: number) => {
  "worklet";

  const y = useSharedValue(0);

  useEffect(() => {
    if (isVisible) {
      y.value = 0;
    } else {
      y.value = textHeight;
    }
  }, [isVisible]);

  return y;
};

interface AnimatedDigitStackProps {
  digit: number;
  textHeight: number;
  state: Animated.SharedValue<ValueState>;
  animationDuration: number;
}

const AnimatedDigitStack = ({
  digit,
  textHeight,
  state,
  animationDuration,
}: AnimatedDigitStackProps) => {
  const y = useUpdateDigitY(digit, textHeight);

  const lastDigit = useRef<number>();
  const lastState = useRef(state.value);

  useEffect(() => {
    lastDigit.current = digit;
    lastState.current = state.value;
  }, [digit]);

  const digitState = useDerivedValue(() => {
    return lastDigit.current !== digit && lastState.current !== ValueState.same
      ? state.value
      : ValueState.same;
  }, []);

  return (
    <AnimatedStack sharedValue={y} {...{ animationDuration, state }}>
      <TenDigitStack state={digitState} />
    </AnimatedStack>
  );
};

interface AnimatedSymbolStackProps {
  symbol: string;
  isVisible: boolean;
  textHeight: number;
  animationDuration: number;
}

const AnimatedSymbolStack = ({
  symbol,
  isVisible,
  textHeight,
  animationDuration,
}: AnimatedSymbolStackProps) => {
  const y = useUpdateSymbolY(isVisible, textHeight);

  return (
    <AnimatedStack sharedValue={y} {...{ animationDuration }}>
      <MeasureElement text={symbol} />
    </AnimatedStack>
  );
};

interface AnimatedStackProps {
  sharedValue: Animated.SharedValue<number>;
  state?: Animated.SharedValue<ValueState>;
  animationDuration: number;
  children: any;
}

const AnimatedStack = ({
  sharedValue,
  state,
  animationDuration,
  children,
}: AnimatedStackProps) => {
  const defaultTimingStyles = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateY: withTiming(
            sharedValue.value,
            {
              duration: animationDuration,
              easing: Easing.out(Easing.exp),
            },
            () => {
              if (state) {
                state.value = ValueState.same;
              }
            }
          ),
        },
      ],
    };
  }, []);

  return (
    <Animated.View style={[styles.tenDigitContainer, defaultTimingStyles]}>
      {children}
    </Animated.View>
  );
};

const useUpdateX = (startX: number, toX: number) => {
  "worklet";

  const x = useSharedValue(startX);

  useEffect(() => {
    x.value = toX;
  }, [toX]);

  return x;
};

interface HorizontalProps {
  startX: number;
  toX: number;
  animationDuration: number;
  children?: any;
}

const Horizontal = ({
  startX,
  toX,
  animationDuration,
  children,
}: HorizontalProps) => {
  const x = useUpdateX(startX, toX);

  const animatedStyle = useAnimatedStyle(() => {
    return {
      transform: [
        {
          translateX: withTiming(x.value, {
            duration: animationDuration,
            easing: Easing.out(Easing.exp),
          }),
        },
      ],
    };
  }, []);

  return <Animated.View style={animatedStyle}>{children}</Animated.View>;
};

const POSSIBLE_STRINGS = [...DIGIT_LIST.map((n) => n.toString()), ",", "."];

const useUpdateValueState = (
  value: Animated.SharedValue<number>,
  useValueState: Animated.SharedValue<boolean>
) => {
  const state = useSharedValue(ValueState.same);

  useAnimatedReaction(
    () => {
      return value.value;
    },
    (current, previous) => {
      if (!useValueState.value || !previous) {
        return;
      }
      if (current === previous) {
        state.value = ValueState.same;
      } else {
        state.value =
          current > previous ? ValueState.increase : ValueState.decrease;
      }
    },
    []
  );

  return state;
};

export default ({
  value,
  fractionDigits = 0,
  animationDuration = 250,
  withGradient = true,
  useValueState = { value: false },
  style,
  debug = false,
}: Props) => {
  const state = useUpdateValueState(value, useValueState);
  const [elements, setElements] = useState<VisualElementType[]>([]);
  const maxPosition = useSharedValue<number>(0);

  useAnimatedReaction(
    () => {
      return value.value;
    },
    (current) => {
      "worklet";
      // Rounding
      const roundedValue = round(current, fractionDigits);

      // Split
      const { whole, fraction } = modf(roundedValue);

      // Example: 123.45
      // Whole: 123 -> [1,2,3]
      // elements = [-1,-1,-1,3,4,5], whole: 123 -> [-1,-1,-1,1,2,3]
      // elements = [-1,-1,-1,3,4,5], whole: 23 -> [-1,-1,-1,-1,2,3]
      const wholeElements = getWholeVisualElements(whole);

      let allElements: VisualElementType[];

      if (fractionDigits > 0) {
        const fractionElements = getDecimalVisualElements(
          fraction,
          fractionDigits
        );
        allElements = [
          ...wholeElements,
          {
            kind: VisualElementKind.dot,
            visible: true,
          } as Dot,
          ...fractionElements,
        ];
      } else {
        allElements = [...wholeElements];
      }

      const diff = maxPosition.value - wholeElements[0]?.position;
      if (diff > 0) {
        let position = maxPosition.value;
        const first = new Array(diff).fill(null).map(
          () =>
            ({
              kind: VisualElementKind.digit,
              value: -1,
              position: position--,
            } as VisualElementType)
        );
        allElements = [...first, ...allElements];
      }

      if (diff < 0) {
        maxPosition.value = wholeElements[0]?.position;
      }

      runOnJS(setElements)(allElements);
    },
    []
  );

  const viewFromElement = (type: VisualElementType): JSX.Element => {
    switch (type.kind) {
      case VisualElementKind.digit:
      case VisualElementKind.decimalDigit:
        return buildDigitStack(type.value);
      default:
        return buildSymbolStack(visualElementString(type), type.visible);
    }
  };

  const buildDigitStack = (digit: number) => {
    return (
      <AnimatedDigitStack
        textHeight={sizes["9"].height}
        {...{
          digit,
          state,
          animationDuration,
        }}
      />
    );
  };

  const buildSymbolStack = (symbol: string, isVisible: boolean) => {
    return (
      <AnimatedSymbolStack
        textHeight={sizes[symbol].height}
        {...{
          symbol,
          isVisible,
          animationDuration,
        }}
      />
    );
  };

  const [sizes, setSizes] = useState<Record<string, DimensionProps>>({});

  const getFirstValidDigitIndex = (): [number, number] | undefined => {
    let startOfFirst = elements.findIndex(
      (element) =>
        element.kind === VisualElementKind.digit && element.position === 1
    );
    for (let i = startOfFirst; i >= 0; i--) {
      const element = elements[i];
      if (
        (element.kind === VisualElementKind.digit && element.value === -1) ||
        (element.kind === VisualElementKind.comma && !element.visible)
      ) {
        return [i, startOfFirst - i];
      }
    }
    return undefined;
  };

  const getX = (index: number) => {
    const firstInvalid = getFirstValidDigitIndex();
    if (firstInvalid !== undefined) {
      const element = elements[index];
      switch (element.kind) {
        case VisualElementKind.digit:
          if (element.value === -1) {
            return -sizes["0"].width * (element.position - firstInvalid[1] + 1);
          }
          break;
        case VisualElementKind.decimalDigit:
          if (element.value === -1) {
            return sizes["0"].width * (element.position - firstInvalid[1] + 1);
          }
          break;
        case VisualElementKind.comma:
          if (!element.visible) {
            return -sizes[","].width * (element.position - firstInvalid[1] + 1);
          }
          break;
      }
    }

    let x = 0;
    for (let i = (firstInvalid?.[0] ?? -1) + 1; i < index; i++) {
      x += sizes[visualElementString(elements[i])].width;
    }
    return x;
  };

  const makeLotteryNumbers = () => {
    return (
      <View
        style={[
          styles.container,
          style,
          {
            overflow: debug ? "visible" : "hidden",
            height: sizes["9"]?.height,
          },
        ]}
      >
        {Object.keys(sizes).length !== POSSIBLE_STRINGS.length && (
          <>
            {POSSIBLE_STRINGS.map((s) => {
              return (
                <View
                  key={s}
                  onLayout={({ nativeEvent: { layout } }) =>
                    setSizes({
                      ...sizes,
                      [s]: { ...layout },
                    })
                  }
                  style={styles.measure}
                >
                  <MeasureElement text={s} />
                </View>
              );
            })}
          </>
        )}

        {Object.keys(sizes).length === POSSIBLE_STRINGS.length &&
          elements.map((element, index) => {
            const toX = getX(index);
            return (
              <View style={styles.element} key={visualElementId(element)}>
                <Horizontal
                  key={visualElementId(element)}
                  startX={-2 * sizes["9"].width}
                  {...{ animationDuration, toX }}
                >
                  {viewFromElement(element)}
                </Horizontal>
              </View>
            );
          })}
      </View>
    );
  };

  return withGradient && !debug ? (
    <MaskedView
      maskElement={
        <LinearGradient
          style={{ flex: 1 }}
          locations={[0, 0.2, 0.8, 1]}
          colors={["transparent", "white", "white", "transparent"]}
        />
      }
    >
      {makeLotteryNumbers()}
    </MaskedView>
  ) : (
    makeLotteryNumbers()
  );
};
