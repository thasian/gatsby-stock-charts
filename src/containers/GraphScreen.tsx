import React, { useCallback, useContext } from "react";
import { SafeAreaView, StyleSheet, View } from "react-native";
import { CandleValues, Footer, Navigation, ThemeContext } from "../components";
import {
  Duration,
  formatCandleDate,
  formatLineDate,
  PlotType,
  useScreen,
} from "../utils";
import { Colors, Typography } from "../theme";
import { CandlestickChart, LineChart } from "../chart";
import Change from "../components/Change";
import ExtraChange from "../components/ExtraChange";
import LotteryNumbersWrapper from "../components/LotteryNumbersWrapper";
import PulseDot from "../components/PulseDot";

const ORDERED_DURATIONS = [
  Duration.day,
  Duration.week,
  Duration.month,
  Duration.months,
  Duration.year,
  Duration.years,
];

const HEADER_BOTTOM_MARGIN = 20;
const HEADER_HORIZONTAL_PADDING = 25;
const NAVIGATION_TOP_MARGIN = 20;
const NAVIGATION_BOTTOM_MARGIN = 20;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: NAVIGATION_TOP_MARGIN,
  },
  header: {
    marginBottom: HEADER_BOTTOM_MARGIN,
    paddingHorizontal: HEADER_HORIZONTAL_PADDING,
  },
  navigation: {
    marginBottom: NAVIGATION_BOTTOM_MARGIN,
    paddingHorizontal: HEADER_HORIZONTAL_PADDING,
  },
});

interface Props {
  stockTicker: string;
}

export default ({ stockTicker }: Props) => {
  const {
    theme: { backgroundColor },
  } = useContext(ThemeContext);

  const {
    activeColor,
    currentDuration,
    currentPlotType,
    lineData,
    candleData,
  } = useScreen();

  const formatGraphLineDate = useCallback(
    ({ value }: any) => {
      "worklet";

      return formatLineDate(currentDuration, value);
    },
    [currentDuration]
  );

  const formatGraphCandleDate = useCallback(
    ({ value, nextValue }: any) => {
      "worklet";

      return formatCandleDate(currentDuration, value, nextValue);
    },
    [currentDuration]
  );

  const showTradingHoursDot = () => {
    if (
      currentDuration !== Duration.day ||
      currentPlotType === PlotType.candle
    ) {
      return false;
    }
    const { data, xValues } = lineData;
    return data.length !== xValues!.length;
  };

  return (
    <SafeAreaView style={[styles.container, { backgroundColor }]}>
      <Navigation style={styles.navigation} {...{ stockTicker }} />

      <LineChart.Provider
        data={lineData.data}
        xValues={lineData.xValues}
        previousDayClose={lineData.previousDayClose}
      >
        <CandlestickChart.Provider
          data={candleData.data}
          xValues={candleData.xValues}
          previousDayClose={candleData.previousDayClose}
        >
          <View style={styles.header}>
            <LotteryNumbersWrapper />
            <Change />
            <ExtraChange label={"After-Hours"} percent={0.05} change={-23} />
            <CandleValues />
          </View>
          <CandlestickChart style={{ marginVertical: 20 }}>
            {currentPlotType === PlotType.line && (
              <LineChart>
                <LineChart.Path color={activeColor} />
                <LineChart.CursorLine color={activeColor}>
                  <LineChart.CursorDateTime
                    format={formatGraphLineDate}
                    textStyle={Typography.RegularBold}
                  />
                </LineChart.CursorLine>
                {currentDuration === Duration.day && (
                  <LineChart.PreviousDayCloseLine />
                )}
                {showTradingHoursDot() && <PulseDot />}
                <CandlestickChart.Volumes
                  positiveColor={Colors.green}
                  negativeColor={Colors.red}
                />
              </LineChart>
            )}
            {currentPlotType === PlotType.candle && (
              <>
                <CandlestickChart.Candles
                  positiveColor={Colors.green}
                  negativeColor={Colors.red}
                />
                <CandlestickChart.Crosshair>
                  <CandlestickChart.Tooltip
                    textStyle={Typography.RegularBold}
                  />
                </CandlestickChart.Crosshair>
                <CandlestickChart.CursorDateTime
                  format={formatGraphCandleDate}
                  textStyle={Typography.RegularBold}
                />
                {currentDuration === Duration.day && (
                  <CandlestickChart.PreviousDayCloseLine />
                )}
                <CandlestickChart.Volumes
                  positiveColor={Colors.green}
                  negativeColor={Colors.red}
                />
              </>
            )}
          </CandlestickChart>
        </CandlestickChart.Provider>
      </LineChart.Provider>

      <Footer durations={ORDERED_DURATIONS} color={activeColor} />
    </SafeAreaView>
  );
};
