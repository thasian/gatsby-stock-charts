import React, { useState } from "react";
import { Colors } from "../theme";
import {
  CandleData,
  Duration,
  DurationToGraphData,
  LineData,
  PlotType,
} from "../utils";

export type TScreenContext = {
  activeColor: string;
  currentDuration: Duration;
  setCurrentDuration: (duration: Duration) => void;
  currentPlotType: PlotType;
  setCurrentPlotType: (plotType: PlotType) => void;
  label: string;
  lineData: LineData;
  candleData: CandleData;
};

export const ScreenContext = React.createContext<TScreenContext>({
  activeColor: "",
  currentDuration: Duration.day,
  setCurrentDuration: () => {},
  currentPlotType: PlotType.line,
  setCurrentPlotType: () => {},
  label: "",
  lineData: {
    data: [],
    valueAccessor: () => 0,
    previousDayClose: undefined,
    xValues: undefined,
  },
  candleData: {
    data: [],
    previousDayClose: undefined,
    xValues: undefined,
  },
});

interface Props {
  durationToGraphData: DurationToGraphData;
  children: React.ReactNode;
}

export function ScreenProvider({ durationToGraphData, children }: Props) {
  const [currentDuration, setCurrentDuration] = useState<Duration>(
    Duration.day
  );
  const [currentPlotType, setCurrentPlotType] = useState<PlotType>(
    PlotType.line
  );

  const contextValue = React.useMemo(() => {
    const graphData = durationToGraphData[currentDuration];
    const lineData = graphData[PlotType.line];
    const candleData = graphData[PlotType.candle];
    const change =
      lineData.data[lineData.data.length - 1].value - lineData.data[0].value;

    return {
      currentDuration,
      setCurrentDuration,
      currentPlotType,
      setCurrentPlotType,
      lineData: lineData,
      candleData: candleData,
      label: graphData.label,
      activeColor: change >= 0 ? Colors.green : Colors.red,
    };
  }, [
    currentDuration,
    setCurrentDuration,
    currentPlotType,
    setCurrentPlotType,
  ]);

  return (
    <ScreenContext.Provider value={contextValue}>
      {children}
    </ScreenContext.Provider>
  );
}
