import React, { useCallback, useContext, useState } from "react";
import {
  Dimensions,
  SafeAreaView,
  StyleSheet,
  Text,
  TextInput,
} from "react-native";
import { Colors, Typography } from "../theme";
import { LotteryNumbers, ValueState, ThemeContext } from "../components";

const height = Dimensions.get("window").height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginLeft: 20,
    marginTop: height / 2,
  },
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});

interface Props {
  width: number;
}

export default ({ width }: Props) => {
  const {
    theme: { textColor },
  } = useContext(ThemeContext);

  const [value, setValue] = useState(0);

  const elementBuilder = useCallback(
    (s: string, state: ValueState) => {
      const color =
        state === ValueState.increase
          ? Colors.green
          : state === ValueState.decrease
          ? Colors.red
          : textColor;
      return (
        <Text
          style={[
            Typography.Title,
            { color, paddingRight: s === "1" ? 4 : s === "2" ? 2 : 0 },
          ]}
        >
          {s}
        </Text>
      );
    },
    [textColor]
  );

  return (
    <SafeAreaView style={styles.container}>
      <LotteryNumbers
        value={value}
        fractionDigits={2}
        elementBuilder={elementBuilder}
        style={{ marginLeft: width / 3 }}
      />
      <TextInput
        style={[styles.input, { borderColor: textColor, color: textColor }]}
        defaultValue={value.toString()}
        onChangeText={(text) => {
          if (Number(text) || text === "0") {
            setValue(Number(text));
          }
        }}
        keyboardType="numeric"
      />
    </SafeAreaView>
  );
};
