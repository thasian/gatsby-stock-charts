import { Duration } from ".";

const DAY_FORMAT_OPTIONS: Intl.DateTimeFormatOptions = {
  hour: "numeric",
  minute: "2-digit",
  hour12: true,
};
const WEEK_FORMAT_OPTIONS: Intl.DateTimeFormatOptions = {
  month: "short",
  day: "numeric",
  hour: "numeric",
  minute: "2-digit",
  hour12: true,
};
const MONTH_FORMAT_OPTIONS: Intl.DateTimeFormatOptions = {
  month: "short",
  day: "numeric",
  year: "numeric",
};
const YEAR_FORMAT_CANDLE_OPTIONS: Intl.DateTimeFormatOptions = {
  month: "short",
  day: "numeric",
};

const durationToFormats = {
  [Duration.day]: {
    general: (date: Date) => {
      "worklet";

      return date.toLocaleString("en-US", DAY_FORMAT_OPTIONS);
    },
    candle: (date: Date) => {
      "worklet";

      let dateString = date.toLocaleString("en-US", DAY_FORMAT_OPTIONS);
      if (dateString !== "11:50 AM") {
        dateString = dateString.slice(0, -3);
      }
      return dateString;
    },
  },
  [Duration.week]: {
    general: (date: Date) => {
      "worklet";

      return date.toLocaleString("en-US", WEEK_FORMAT_OPTIONS);
    },
    candle: (date: Date) => {
      "worklet";

      return date.toLocaleString("en-US", WEEK_FORMAT_OPTIONS).slice(0, -3);
    },
  },
  [Duration.month]: {
    general: (date: Date) => {
      "worklet";

      return date.toLocaleString("en-US", MONTH_FORMAT_OPTIONS);
    },
  },
  [Duration.months]: {
    general: (date: Date) => {
      "worklet";

      return date.toLocaleString("en-US", MONTH_FORMAT_OPTIONS);
    },
  },
  [Duration.year]: {
    general: (date: Date) => {
      "worklet";

      return date.toLocaleString("en-US", MONTH_FORMAT_OPTIONS);
    },
    candle: (date: Date) => {
      "worklet";

      return date.toLocaleString("en-US", YEAR_FORMAT_CANDLE_OPTIONS);
    },
  },
  [Duration.years]: {
    general: (date: Date) => {
      "worklet";

      return date.toLocaleString("en-US", MONTH_FORMAT_OPTIONS);
    },
    candle: (date: Date) => {
      "worklet";

      return date.toLocaleString("en-US", YEAR_FORMAT_CANDLE_OPTIONS);
    },
  },
};

/**
 * FROM: https://stackoverflow.com/a/1214753
 *
 * Adds time to a date. Modelled after MySQL DATE_ADD function.
 * Example: dateAdd(new Date(), 'minute', 30)  //returns 30 minutes from now.
 * https://stackoverflow.com/a/1214753/18511
 *
 * @param date  Date to start with
 * @param interval  One of: year, quarter, month, week, day, hour, minute, second
 * @param units  Number of units of the given interval to add.
 */
function dateAdd(date: Date, interval: string, units: number) {
  "worklet";

  if (!(date instanceof Date)) return new Date();
  var ret = new Date(date); //don't change original date
  var checkRollover = function () {
    if (ret.getDate() != date.getDate()) ret.setDate(0);
  };
  switch (String(interval).toLowerCase()) {
    case "year":
      ret.setFullYear(ret.getFullYear() + units);
      checkRollover();
      break;
    case "quarter":
      ret.setMonth(ret.getMonth() + 3 * units);
      checkRollover();
      break;
    case "month":
      ret.setMonth(ret.getMonth() + units);
      checkRollover();
      break;
    case "week":
      ret.setDate(ret.getDate() + 7 * units);
      break;
    case "day":
      ret.setDate(ret.getDate() + units);
      break;
    case "hour":
      ret.setTime(ret.getTime() + units * 3600000);
      break;
    case "minute":
      ret.setTime(ret.getTime() + units * 60000);
      break;
    case "second":
      ret.setTime(ret.getTime() + units * 1000);
      break;
    default:
      ret = new Date();
      break;
  }
  return ret;
}

export const formatLineDate = (duration: Duration, day: number) => {
  "worklet";

  const { general } = durationToFormats[duration];
  return general(new Date(day));
};

const durationToUseNextDate = {
  [Duration.day]: (current: Date, next: Date) => {
    "worklet";

    return current.getDay() !== next.getDay();
  },
  [Duration.week]: (current: Date, next: Date) => {
    "worklet";

    return current.getDay() !== next.getDay();
  },
};

const durationToNextDate = {
  [Duration.day]: (last: number) => {
    "worklet";

    return dateAdd(new Date(last), "minute", 10);
  },
  [Duration.week]: (last: number) => {
    "worklet";

    return dateAdd(new Date(last), "hour", 1);
  },
  [Duration.year]: (last: number) => {
    "worklet";

    return dateAdd(new Date(last), "week", 1);
  },
  [Duration.years]: (last: number) => {
    "worklet";

    return dateAdd(new Date(last), "month", 1);
  },
};

export const formatCandleDate = (
  duration: Duration,
  day: number,
  next?: number
) => {
  "worklet";

  const { general, candle } = durationToFormats[duration];
  if (candle === undefined) {
    return general(new Date(day));
  }
  if (next !== undefined) {
    const dayDate = new Date(day);
    const nextDate = new Date(next);
    if (durationToUseNextDate[duration]?.(dayDate, nextDate)) {
      return (
        candle(dayDate) + " - " + general(durationToNextDate[duration](day))
      );
    }
    return candle(dayDate) + " - " + general(nextDate);
  }
  return (
    candle(new Date(day)) + " - " + general(durationToNextDate[duration](day))
  );
};
