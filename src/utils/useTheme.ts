import { useContext } from "react";

import { ThemeContext } from "../components";

export const useTheme = () => {
  return useContext(ThemeContext);
};
