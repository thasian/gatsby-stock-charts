import day from "../data/1d.json";
import week from "../data/1w.json";
import month from "../data/1m.json";
import months from "../data/3m.json";
import year from "../data/1y.json";
import years from "../data/5y.json";
import dayjs from "dayjs";
import { TCandle, TLineChartPoint } from "../chart";

// Model we use to parse the JSON
interface Value {
  datetime: string;
  open: string;
  high: string;
  low: string;
  close: string;
  volume: string;
}

// Globally used interfaces
export interface DimensionProps {
  width: number;
  height: number;
}

export const Duration = {
  day: "1D",
  week: "1W",
  month: "1M",
  months: "3M",
  year: "1Y",
  years: "5Y",
};
export type Duration = typeof Duration[keyof typeof Duration];

export enum PlotType {
  candle,
  line,
}

export interface DataPoint {
  date: Date;
  endDate?: Date;
  open: number;
  high: number;
  low: number;
  close: number;
  volume: number;
}

/*
  Ideally, you would use the ticker to make an API
  request for data. However, to allow consistency in
  this project, we just use cached JSON data.
*/
export const makeDurationToGraphData = (isTradingHours: boolean) => ({
  [Duration.day]: {
    label: "Today",
    ...buildGraph(Duration.day, isTradingHours),
  },
  [Duration.week]: {
    label: "Past Week",
    ...buildGraph(Duration.week, false),
  },
  [Duration.month]: {
    label: "Past Month",
    ...buildGraph(Duration.month, false),
  },
  [Duration.months]: {
    label: "Past 3 Months",
    ...buildGraph(Duration.months, false),
  },
  [Duration.year]: {
    label: "Past Year",
    ...buildGraph(Duration.year, false),
  },
  [Duration.years]: {
    label: "Past 5 Years",
    ...buildGraph(Duration.years, false),
  },
});

export type DurationToGraphData = ReturnType<typeof makeDurationToGraphData>;
export type LineData = ReturnType<typeof buildLineGraph>;
export type CandleData = ReturnType<typeof buildCandleGraph>;

const durationToJson = {
  [Duration.day]: day,
  [Duration.week]: week,
  [Duration.month]: month,
  [Duration.months]: months,
  [Duration.year]: year,
  [Duration.years]: years,
};

const buildGraph = (duration: Duration, isTradingHours: boolean) => {
  const data = durationToJson[duration];
  const values = data.values as Value[];
  // The datapoints are from new->old so we want old->new
  const datapoints = makeDataPoints([...values].reverse());

  const valueAccessor = (d: DataPoint): number => {
    if (duration === Duration.day || duration === Duration.week) {
      return d.open;
    } else {
      return d.close;
    }
  };

  const getPreviousDayLast = () => {
    if (duration !== Duration.day) {
      return undefined;
    }
    const weekData = durationToJson[Duration.week];
    const weekValues = weekData.values as Value[];
    const weekDatapoints = makeDataPoints([...weekValues].reverse());

    return weekDatapoints[weekDatapoints.length - 1];
  };

  const previousDayLast = getPreviousDayLast();

  return {
    [PlotType.line]: {
      ...buildLineGraph(
        datapoints.slice(0, isTradingHours ? 30 : undefined),
        valueAccessor,
        duration === Duration.day ? makeTodayDates(5) : undefined,
        previousDayLast
      ),
    },
    [PlotType.candle]: {
      ...buildCandleGraph(
        makeCandleDataPoints(
          datapoints.slice(0, isTradingHours ? 30 : undefined),
          durationToAddTime[duration],
          durationToStep[duration],
          durationToEndEarly[duration]
        ),
        duration === Duration.day ? makeTodayDates(10).slice(0, -1) : undefined,
        previousDayLast
      ),
    },
  };
};

const makeDataPoints = (values: Value[]): DataPoint[] => {
  return values.map(({ datetime, open, high, low, close, volume }) => ({
    date: dayjs(datetime).toDate(),
    open: parseFloat(open),
    high: parseFloat(high),
    low: parseFloat(low),
    close: parseFloat(close),
    volume: parseFloat(volume),
  }));
};

export const getMarketOpenDate = () => {
  const day = dayjs("2021-07-30 09:30:00");
  return day;
};

export const getMarketEndDate = () => {
  const day = dayjs("2021-07-30 16:00:00");
  return day;
};

const makeTodayDates = (step: number) => {
  let day = getMarketOpenDate();
  const end = getMarketEndDate();

  let date: Date[] = [];

  while (!day.isAfter(end)) {
    date.push(day.toDate());
    day = day.add(step, "m");
  }

  return date;
};

// Line
const buildLineGraph = (
  data: DataPoint[],
  valueAccessor: (d: DataPoint) => number,
  xValues?: Date[],
  previousDayLast?: DataPoint
) => {
  const lineData: TLineChartPoint[] = data.map((d) => ({
    timestamp: d.date.getTime(),
    value: valueAccessor(d),
  }));

  const previousDayClose = previousDayLast ? previousDayLast.close : undefined;

  return {
    data: lineData,
    xValues: xValues?.map((date) => date.getTime()) ?? undefined,
    previousDayClose,
    valueAccessor,
  };
};

// Candle
const buildCandleGraph = (
  data: DataPoint[],
  xValues?: Date[],
  previousDayLast?: DataPoint
) => {
  const candleData: TCandle[] = data.map((d) => ({
    timestamp: d.date.getTime(),
    ...d,
  }));

  const previousDayClose = previousDayLast ? previousDayLast.close : undefined;

  return {
    data: candleData,
    xValues: xValues?.map((date) => date.getTime()) ?? undefined,
    previousDayClose,
  };
};

interface AddTime {
  value: number;
  unit: dayjs.OpUnitType;
}

const durationToAddTime: Record<Duration, AddTime> = {
  [Duration.day]: { value: 10, unit: "m" },
  [Duration.week]: { value: 1, unit: "h" },
  [Duration.year]: { value: 1, unit: "w" },
  [Duration.years]: { value: 1, unit: "M" },
};

const durationToStep = {
  [Duration.day]: 2,
  [Duration.week]: 4,
  [Duration.month]: 0,
  [Duration.months]: 0,
  [Duration.year]: 5,
  [Duration.years]: 4,
};

const durationToEndEarly = {
  [Duration.week]: (a: Date, b: Date): boolean => {
    return a.getDay() !== b.getDay();
  },
};

const makeCandleDataPoints = (
  values: DataPoint[],
  addTime: AddTime,
  step: number,
  endEarly?: (a: Date, b: Date) => boolean
): DataPoint[] => {
  if (step === 0) {
    return values;
  }

  let counter = step - 1;
  let current = values[0];
  let candles: DataPoint[] = [];

  const endCycle = (i: number, endDate: Date) => {
    counter = step;
    current.endDate = endDate;
    current.close = values[i - 1].close;
    candles.push(current);
    current = values[i];
  };

  const makeEndDate = (): Date => {
    const { value, unit } = addTime;
    const day = dayjs(current.date);
    return day.add(value, unit).toDate();
  };

  for (let i = 1; i < values.length - 1; i++) {
    const { date, high, low, volume } = values[i];

    if (endEarly?.(values[i].date, current.date)) {
      endCycle(i, makeEndDate());
    } else if (counter === 0) {
      endCycle(i, date);
    } else {
      current.high = Math.max(current.high, high);
      current.low = Math.min(current.low, low);
      current.volume += volume;
    }
    counter--;
  }

  endCycle(values.length, makeEndDate());

  return candles;
};

// Exported types for usage in component props
export type GraphCore = ReturnType<typeof buildGraph>;
export type LineGraphCore = ReturnType<typeof buildLineGraph>;
export type CandleGraphCore = ReturnType<typeof buildCandleGraph>;
