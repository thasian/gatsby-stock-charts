export const range = (
  start: number,
  length: number,
  step: number
): number[] => {
  let ranges = [];
  for (let i = 0, n = start; i < length; i++, n += step) {
    ranges.push(n);
  }
  return ranges;
};

function numberWithCommas(x: number, nDigits: number) {
  "worklet";

  return x.toLocaleString("en-US", {
    minimumFractionDigits: nDigits,
    maximumFractionDigits: nDigits,
  });
}

export const toUSD = (n: number): string => {
  "worklet";

  return "$" + numberWithCommas(n, 2);
};

export const toPercent = (n: number): string => {
  "worklet";

  const prefix = n > 0 ? "+" : "";

  return prefix + numberWithCommas(n * 100, 2) + "%";
};

export const toBigNumber = (n: number): string => {
  "worklet";

  if (n <= 10000) {
    return numberWithCommas(n, 2);
  }
  if (n <= 1000000) {
    return numberWithCommas(n, 0);
  }

  const lookup = [
    { value: 1e6, symbol: "M" },
    { value: 1e9, symbol: "B" },
  ];
  const rx = /\.0+$|(\.[0-9]*[1-9])0+$/;
  var item = lookup
    .slice()
    .reverse()
    .find(function (item) {
      return n >= item.value;
    });
  return item
    ? (n / item.value).toFixed(2).replace(rx, "$1") + item.symbol
    : "0";
};

export const modf = (n: number) => {
  "worklet";

  const whole = Math.trunc(n);
  const fraction = n - whole;
  return { whole, fraction };
};

export const round = (n: number, numPlaces: number): number => {
  "worklet";

  const power = Math.pow(10, numPlaces);
  return Math.round((n + Number.EPSILON) * power) / power;
};
