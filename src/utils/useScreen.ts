import { useContext } from "react";

import { ScreenContext } from "../containers";

export const useScreen = () => {
  return useContext(ScreenContext);
};
