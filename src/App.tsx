import React from "react";
import {
  GraphScreen,
  LotteryNumbersTestScreen,
  ScreenProvider,
} from "./containers";
import { ThemeProvider } from "./components";
import { makeDurationToGraphData } from "./utils";

export default () => {
  return (
    <ThemeProvider>
      <ScreenProvider durationToGraphData={makeDurationToGraphData(true)}>
        <GraphScreen stockTicker={"TSLA"} />
      </ScreenProvider>
      {/* <LotteryNumbersTestScreen {...{ width }} /> */}
    </ThemeProvider>
  );
};
