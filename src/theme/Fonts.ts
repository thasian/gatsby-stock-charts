import { TextStyle } from "react-native";

export const Typography = {
  Title: {
    fontSize: 32,
    fontFamily: "Poppins-Bold",
    includeFontPadding: false,
    textAlignVertical: "center",
  } as TextStyle,
  Regular: {
    fontSize: 13,
    fontFamily: "Poppins-Regular",
    includeFontPadding: false,
    textAlignVertical: "center",
  } as TextStyle,
  RegularBold: {
    fontSize: 13,
    fontFamily: "Poppins-SemiBold",
    includeFontPadding: false,
    textAlignVertical: "center",
  } as TextStyle,
  H2: {
    fontSize: 15,
    fontFamily: "Poppins-SemiBold",
    includeFontPadding: false,
    textAlignVertical: "center",
  } as TextStyle,
  Body: {
    fontSize: 13,
    fontFamily: "Poppins-Regular",
    includeFontPadding: false,
    textAlignVertical: "center",
  } as TextStyle,
  Caption: {
    fontSize: 12,
    fontFamily: "Poppins-Light",
    includeFontPadding: false,
    textAlignVertical: "center",
  } as TextStyle,
};
